import { useState } from 'react'
import Pokedex from './components/Pokedex'
import HomePage from './pages/HomePage'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './App.css'

function App() {
  const [id, setId] = useState(0)
  return (
    <>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage/>}/>
          <Route path="/pokedex/:id" element={<Pokedex/>}/>
      </Routes>
    </BrowserRouter>
    </>
  )
}

export default App
