import '../App.css'
function PokemonCard({ name, imgSrc, types }) {
    const allTypes = types.map((value,index) => {
        return(
            <h6 key={index}>{value}</h6>
        )
    })
return (
    <>
    <div className="card-main">
        <div className="card-image">
            <img src={imgSrc}></img>
        </div>
        <div className="card-name">
            <h4>{name}</h4>
        </div>
        <div className="card-type">
            {allTypes}
        </div>
    </div>
    </>
)
}

export default PokemonCard;