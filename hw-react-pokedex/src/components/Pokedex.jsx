import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom';
import getPokemonData from '../hooks/getPokemonData';
import PokemonCard from './PokemonCard';
import '../App.css'

function Pokedex () {
    const { id } = useParams();
    // Cache load pokemon data since querying in small batch is very slow
    const cachePokemon = 20;
    // Select number of pokemon to be seen in window then load more
    const viewPokemon = 10;
    // Pokemon start id
    const initialPokemon = 1;
    const idInitial = parseInt(id);
    // useState variables
    const [count, setCount] = useState(idInitial);
    const [start, setStart] = useState(idInitial - 1); // -1 since json index starts at 0 while pokemon api is at 1
    const [stop, setStop] = useState(idInitial * viewPokemon - 1); // -1 since json index starts at 0 while pokemon api is at 1
    // Use dynamic routing to find pokemon
    useEffect(() => {
        if(id) {
            setCount(idInitial)
            setStart(((idInitial - 1) * viewPokemon));
            setStop(((idInitial - 1) * viewPokemon) + viewPokemon - 1);
        }
    },[id])
    // Event Handler for submit button
    const setCountHandler = () => {
        setCount(count + 1);
        setStart((count * viewPokemon));
        setStop((count * viewPokemon) + viewPokemon - 1);
    }
    // Create Pokedex from card component of each pokemon
    const createPokedex = getPokemonData(initialPokemon, stop + cachePokemon)
            .pokemonData.map((value, index) => {
                if (index >= initialPokemon - 1 && index <= stop)
                return <PokemonCard name = {value.name} imgSrc = {value.imgSrc} types = {value.types} key={index}/>
            })
    return (
        <>  
            <h1>My Pokedex</h1>
            <h4>Can take around 5 seconds for initial load</h4>
            <button className="load-button" onClick={setCountHandler}>Load More</button>
            <div className="pokedex-main">
                {createPokedex}
            </div>
        </>
    )
}
export default Pokedex;