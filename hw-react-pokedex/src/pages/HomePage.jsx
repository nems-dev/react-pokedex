import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import '../App.css'

function HomePage() {
    const navigate = useNavigate();
    const [id, setId] = useState(1);
    const setIdHandler = (event) => {
        setId(event.target.value);
    }
    const navigateHandler = (event) => {
        event.preventDefault();
        navigate(`/pokedex/${id}`);
    }
    return (
        <>
        <h1>Welcome to my Pokedex</h1>
        <button className = "load-button" onClick={navigateHandler}>Go to Pokedex</button>
        <label>
        <br/> <br/>
            Testing dynamic routing <br/>
            Set batch of pokemon (can be 1,2,3 ...)
        <br/> <br/>
        </label>
        <input type="text" onChange={setIdHandler} value={id}/>
        </>
    )
}
export default HomePage;