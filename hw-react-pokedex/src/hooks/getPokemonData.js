import { useState, useEffect } from 'react';

const getPokemonData = (start,stop) => {
    // // Vanilla JS converted to custom hook co ChatGPT
    // let finalData = [];
    // for (let i=start;i<=stop;i++) {
    //     let temp = [];
    //     let pokedata = getData(`https://pokeapi.co/api/v2/pokemon/${i}`)
    //     if (!pokedata.isLoading){
    //         pokedata.data.types.forEach(value => temp.push(value.type.name))
    //         finalData.push({name: pokedata.data.name, types: temp})
    //     }
    // }
    const [pokemonData, setPokemonData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    useEffect(() => {
      async function fetchData() {
        let finalData = [];
        for (let i = start; i <= stop; i++) {
          let temp = [];
          try {
            const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${i}`);
            const pokedata = await response.json();
            pokedata.types.forEach((value) => temp.push(value.type.name));
            finalData.push({ name: pokedata.name, imgSrc: pokedata.sprites.front_default, types: temp });
          } catch (error) {
            console.error(`Error fetching data for Pokemon ${i}: ${error}`);
          }
        }
        setPokemonData(finalData);
        setIsLoading(false);
      }
      fetchData();
    }, [start, stop]);
    return { isLoading, pokemonData };
}

export default getPokemonData;